To start the project you need to run:

`npm i`

`npm run start`

To create an image and run the container, you need to run the following command from the project folder:

`./start.sh`

If an error occurs with the launch of the file, then you must give the file permissions to run:

`chmod +x ./start.sh`

When executing a push to the master branch, an automatic start occurs `npm run test`