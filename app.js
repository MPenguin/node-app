const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

let userCounter = 0;

const server = http.createServer(( req, res ) => {

  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');

  if(req.url !== '/favicon.ico'){

    userCounter++;
  }

  res.end(`Hello!\nWe have had ${userCounter} visits!\n`);

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});